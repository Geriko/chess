using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System.Linq;

namespace Geriko.Boardgame
{
    public class GameController : MonoBehaviour
    {
        public static GameController Instance;
        [Header("Settings")]
        public GameStartType GameStart;
        [SerializeField]
        private Config config;

        [Header("Managers")]
        public BoardManager boardManager;
        public PlayerManager playerManager;
        public RoundManager roundManager;
        [SerializeField]
        private GameCamera gameCamera;

        private Field selectedField;
        private Character selectedCharacter;

        internal Player GetNextPlayer()
        {
            int playerIndex = playerManager.Players.IndexOf(roundManager.CurrentPlayer);

            playerIndex++;
            if (playerIndex >= playerManager.Players.Count)
            {
                playerIndex = 0;
            }

            return playerManager.Players[playerIndex];
        }

        private void Awake()
        {
            if(Instance == null)
            {
                Instance = this;
            }
            else
            {
                Debug.LogWarning("An older GameController has been replaced by this instance!");
                Instance = this;
            }

            if (GameStart == GameStartType.Awake)
            {
                SetupGame();
            }
        }
        private void Start()
        {
            if (GameStart == GameStartType.Start)
            {
                SetupGame();
            }
        }

        internal Player GetPlayerWithId(int playerId)
        {
            return playerManager.Players.FirstOrDefault(x => x.Id == playerId);
        }

        internal Character GetSelectedCharacter()
        {
            return selectedCharacter;
        }


        public Config GetConfig()
        {
            return config;
        }

        public void SetupGame()
        {
            //Creating players
            for (int i = 0; i < config.players.Count; i++)
            {
                playerManager.CreatePlayer(config.players[i]);
            }

            //Setting up board
            boardManager.SetupTable(config);

            //Init camera
            gameCamera.InitCamera(config.boardWidth, config.boardHeight, config.fieldHeight, config.fieldWidth, config.boardPadding);

            //Start first round
            roundManager.StartFirstRound();
        }

        #region Events

        internal void OnScreenTouch(Vector2 inputPosition)
        {
            Transform objectHit = RaycastFromCameraToPosition(inputPosition);
            if (objectHit)
            {
                Debug.Log("Hit : " + objectHit.name);

                Field field = objectHit.GetComponent<Field>();

                if (field != null)
                {
                    switch (field.GetState())
                    {
                        case FieldState.Idle:
                            DeselectField();
                            SelectField(field);
                            break;
                        case FieldState.Selected:
                            DeselectField();
                            break;
                        case FieldState.Highlighted:
                            ExcecutePlayerAction(field);
                            DeselectField();
                            roundManager.NextRound();
                            break;
                    }
                }
            }
        }

        internal virtual void DeselectField()
        {
            selectedField = null;
            selectedCharacter = null;
            boardManager.DeselectAll();
        }

        internal virtual void ExcecutePlayerAction(Field field)
        {
            selectedField = field;
            if (selectedCharacter)
            {
                if (selectedField.GetState() == FieldState.Highlighted)
                {
                    selectedField.GetTargetAction().Excecute(selectedCharacter, selectedField);
                }
            }
        }

        internal virtual void SelectField(Field field)
        {
            selectedField = field;
            field.Select();
            selectedCharacter = field.GetCharacter();
            if (selectedCharacter != null)
            {
                if (selectedCharacter.playerId == roundManager.CurrentPlayer.Id)
                {
                    //Player selected it's own character
                    boardManager.SelectCharacter(selectedCharacter, field);
                }
                else
                {
                    //Player selected enemy character
                }
            }
        }

        #endregion

        public Transform RaycastFromCameraToPosition(Vector2 inputPosition)
        {
            RaycastHit hit;
            Ray ray = gameCamera.Camera.ScreenPointToRay(inputPosition);

            if (Physics.Raycast(ray, out hit))
            {
                return hit.transform;
            }
            return null;
        }
    }
}
