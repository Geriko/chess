using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geriko
{
    public static class CollectionExtansions
    {
        public static bool IsExist<T>(this T[,] matrix, int i, int j)
        {
            bool isExist = i >= 0 &&
                   j >= 0 &&
                   i < matrix.GetLength(0) &&
                   j < matrix.GetLength(1);

            return isExist;
        }

        public static List<T> Copy<T> (this List<T> list)
        {
            List<T> newList = new List<T>(list.Count);

            list.ForEach((item) =>
            {
                newList.Add(item);
            });
            return newList;
        }
    }
}