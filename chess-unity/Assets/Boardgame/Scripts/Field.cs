using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geriko.Boardgame
{
    public class Field : MonoBehaviour
    {
        private FieldState state = FieldState.Idle;
        private Action targetAction;
        public Renderer highlight;
        public Vector2Int Coordinate { get { return coordinate; } }
        private Vector2Int coordinate;
        private Character character;

        public static Vector2 Distance(Field from, Field to)
        {
            return new Vector2(to.coordinate.x - from.coordinate.x, to.coordinate.y - from.coordinate.y);
        }

        public FieldState GetState()
        {
            return state;
        }

        public virtual void InitField(int x, int y)
        {
            coordinate.x = x;
            coordinate.y = y;
        }

        internal Action GetTargetAction()
        {
            return targetAction;
        }

        public virtual Character GetCharacter()
        {
            return character;
        }

        internal void AddCharacter(Character ch)
        {
            character = ch;
            character.SetFieldCoordinate(coordinate);
        }

        internal void RemoveCharacter()
        {
            character = null;
        }

        public virtual void Select()
        {
            state = FieldState.Selected;
        }

        internal virtual void Highlight(Action action)
        {
            targetAction = action;
            state = FieldState.Highlighted;
            highlight.enabled = true;
        }

        internal virtual void Dehighlight()
        {
            highlight.enabled = false;
            state = FieldState.Idle;
        }
    }
}
