using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geriko.Boardgame
{
    public enum GameStartType { Awake = 0, Start = 1, None = 3 }
    public enum FieldState { Idle = 0, Selected = 1, Highlighted = 2 }
    public enum Blockable { None = 0, Enemy = 1, Friendly = 2, Everything = 3 }
}