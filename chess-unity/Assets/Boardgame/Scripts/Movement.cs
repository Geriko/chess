﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Geriko.Boardgame
{
    [System.Serializable]
    internal class Movement : Action
    {
        public override void RevealActions(Character character, Field currentField, Field[,] fields, Config config)
        {
            List<Field> possibleField = PossibleFields(character, currentField, fields, config);
            for (int i = 0; i < possibleField.Count; i++)
            {
                possibleField[i].Highlight(this);
            }
        }

        public virtual List<Field> PossibleFields(Character character, Field currentField, Field[,] fields, Config config)
        {
            List<Field> result = new List<Field>();
            for (int x = 0; x < config.boardWidth; x++)
            {
                for (int y = 0; y < config.boardHeight; y++)
                {
                    if (currentField != fields[x, y])
                        result.Add(fields[x, y]);
                }
            }
            return result;
        }

        internal override void Excecute(Character character, Field targetField)
        {
            GameController.Instance.boardManager.DeatachCharacter(character);
            character.transform.position = GameController.Instance.boardManager.CenterOfField(targetField);
            targetField.AddCharacter(character);
        }
    }

    [System.Serializable]
    internal class LinearMovement : Movement
    {
        public Blockable blockable;
        public Vector2Int direction;
        public int maxDistance;
        public int minDistance;

        public override List<Field> PossibleFields(Character character, Field currentField, Field[,] fields, Config config)
        {
            List<Field> result = new List<Field>();
            for (int j = minDistance; j <= maxDistance; j++)
            {
                int x = currentField.Coordinate.x + (direction.x * j);
                int y = currentField.Coordinate.y + (direction.y * j);
                if (fields.IsExist(x, y))
                {
                    Character characterOnNextField = fields[x, y].GetCharacter();
                    if (characterOnNextField != null)
                    {
                        bool isEnemy = characterOnNextField.playerId != character.playerId;
                        switch (blockable)
                        {
                            case Blockable.None:
                                break;
                            case Blockable.Enemy:
                                if (isEnemy)
                                {
                                    return result;
                                }
                                break;
                            case Blockable.Friendly:
                                if (!isEnemy)
                                {
                                    return result;
                                }
                                break;
                            case Blockable.Everything:
                                return result;
                        }
                    }
                    result.Add(fields[x, y]);
                }
            }
            return result;
        }
    }

    [System.Serializable]
    internal class LinearMovementGroup : Movement
    {
        public Blockable blockable;
        public List<Vector2Int> directions;
        public int maxDistance;
        public int minDistance;
        
        public override List<Field> PossibleFields(Character character, Field currentField, Field[,] fields, Config config)
        {
            List<Field> result = new List<Field>();
            foreach (Vector2Int direction in directions)
            {
                for (int j = minDistance; j <= maxDistance; j++)
                {
                    int x = currentField.Coordinate.x + (direction.x * j);
                    int y = currentField.Coordinate.y + (direction.y * j);
                    if (fields.IsExist(x, y))
                    {
                        Character characterOnNextField = fields[x, y].GetCharacter();
                        if (characterOnNextField != null)
                        {
                            bool isDone = false;
                            bool isEnemy = characterOnNextField.playerId != character.playerId;
                            switch (blockable)
                            {
                                case Blockable.None:
                                    break;
                                case Blockable.Enemy:
                                    if (isEnemy)
                                    {
                                        isDone = true;
                                        break;
                                    }
                                    break;
                                case Blockable.Friendly:
                                    if (!isEnemy)
                                    {
                                        isDone = true;
                                        break;
                                    }
                                    break;
                                case Blockable.Everything:
                                    isDone = true;
                                    break;
                            }
                            if (isDone)
                            {
                                break;
                            }
                        }
                        result.Add(fields[x, y]);
                    }
                }
            }
            return result;
        }
    }

    [System.Serializable]
    internal class JumpPositionGroup : Movement
    {
        internal List<Vector2Int> jumpPositions;

        public override List<Field> PossibleFields(Character character, Field currentField, Field[,] fields, Config config)
        {
            List<Field> result = new List<Field>();

            foreach (Vector2Int position in jumpPositions)
            {
                int x = currentField.Coordinate.x + position.x;
                int y = currentField.Coordinate.y + position.y;
                if (fields.IsExist(x, y))
                {
                    if (Jumpable(fields[x, y], character))
                    {
                        result.Add(fields[x, y]);
                    }
                }
            }
            return result;
        }

        internal virtual bool Jumpable(Field field, Character character)
        {
            return true;
        }
    }
}