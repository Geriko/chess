using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geriko.Boardgame
{
    [System.Serializable]
    public class Action
    {
        public float cost;

        public virtual void RevealActions(Character character, Field currentField, Field[,] fields, Config config)
        {
            return;
        }

        internal virtual void Excecute(Character character, Field targetField)
        {
            return;
        }
    }
}
