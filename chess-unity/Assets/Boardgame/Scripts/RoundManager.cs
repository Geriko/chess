using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Geriko.Boardgame
{
    public class RoundManager : MonoBehaviour
    {
        internal Player CurrentPlayer { get { return currentPlayer; } }
        private Player currentPlayer;

        #region Events
        internal System.Action OnEndOfRound;
        #endregion

        public virtual void StartFirstRound()
        {
            List<Player> players = GameController.Instance.playerManager.Players;
            currentPlayer = players[0];
        }

        public virtual void NextRound()
        {
            OnEndOfRound?.Invoke();

            List<Player> players = GameController.Instance.playerManager.Players;
            
            int playerIndex = players.IndexOf(CurrentPlayer);

            playerIndex++;
            if (playerIndex >= players.Count)
            {
                playerIndex = 0;
            }

            currentPlayer = players[playerIndex];
        }
    }
}