using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geriko.Boardgame
{
    public class Player : MonoBehaviour
    {
        internal int Id { get { return id; } }
        private int id;

        private List<Character> characters;

        public void InitPlayer(PlayerConfig config)
        {
            id = config.id;
        }

        public List<Character> GetCharacters()
        {
            return characters;
        }

        public void AddCharacter(Character character)
        {
            if (characters == null) characters = new List<Character>();

            characters.Add(character);
        }
    }
}
