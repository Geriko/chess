using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Geriko.Boardgame
{
    public class BoardManager : MonoBehaviour
    {
        private Config currentConfig;
        internal Field[,] Fields { get { return fields; } }
        private Field[,] fields;

        public void SetupTable(Config config)
        {
            currentConfig = config;

            CreateSquareBoard();
            PlaceCharacters();
        }

        private void PlaceCharacters()
        {
            for (int i = 0; i < currentConfig.prePositionedCharacters.Count; i++)
            {
                SpawnCharacter(currentConfig.prePositionedCharacters[i]);
            }
        }

        private void SpawnCharacter(PrePositionedCharacter character)
        {
            GameObject characterGo = Instantiate(character.prefab);
            Character ch = characterGo.GetComponent<Character>();
            characterGo.transform.position = CenterOfField(fields[character.position.x, character.position.y]);
            fields[character.position.x, character.position.y].AddCharacter(ch);
            ch.playerId = character.PlayerId;
            ch.Init();
            Player player = GameController.Instance.GetPlayerWithId(character.PlayerId);
            player.AddCharacter(ch);
        }
        
        internal virtual void SelectCharacter(Character character, Field field)
        {
            Debug.Log("Select character : " + character.name);
            RevealActions(character, field);
        }

        private void RevealActions(Character character, Field field)
        {
            List<Action> actions = character.GetActions();
            for (int i = 0; i < actions.Count; i++)
            {
                actions[i].RevealActions(character, field, fields, currentConfig);
            }
        }

        private void CreateSquareBoard()
        {
            fields = new Field[currentConfig.boardWidth, currentConfig.boardHeight];

            for (int x = 0; x < currentConfig.boardWidth; x++)
            {
                for (int y = 0; y < currentConfig.boardHeight; y++)
                {
                    GameObject fieldGo = Instantiate(currentConfig.fieldPrefab, transform);
                    fields[x, y] = fieldGo.GetComponent<Field>();

                    fieldGo.transform.localPosition = new Vector3(x * currentConfig.fieldWidth, 0, y * currentConfig.fieldHeight);

                    fields[x, y].InitField(x, y);

                    fieldGo.name = "Field(" + x + "," + y + ")";
                }
            }

            StaticBatchingUtility.Combine(gameObject);
        }

        internal Field GetCurrentFieldOf(Character character)
        {
            for (int x = 0; x < fields.GetLength(0); x++)
            {
                for (int y = 0; y < fields.GetLength(1); y++)
                {
                    if (fields[x, y].GetCharacter() == character)
                        return fields[x, y];
                }
            }
            return null;
        }

        internal Field GetCurrentFieldOf(Vector2Int coordinate)
        {
            if (fields.IsExist(coordinate.x, coordinate.y))
            {
                return fields[coordinate.x, coordinate.y];
            }
            return null;
        }

        internal void DeselectAll()
        {
            for (int x = 0; x < currentConfig.boardWidth; x++)
            {
                for (int y = 0; y < currentConfig.boardHeight; y++)
                {
                    fields[x, y].Dehighlight();
                }
            }
        }

        internal void DeatachCharacter(Character character)
        {
            for (int i = 0; i < fields.GetLength(0); i++)
            {
                for (int j = 0; j < fields.GetLength(1); j++)
                {
                    if(fields[i,j].GetCharacter() == character)
                    {
                        Debug.Log("Character deatached");
                        fields[i, j].RemoveCharacter();
                    }
                }
            }
        }

        public Vector3 CenterOfField(Field field)
        {
            if(currentConfig == null)
            {
                Debug.LogError("Current config is null. Call SetupTable method before this!");
                return Vector3.zero;
            }

            return field.transform.position + new Vector3(currentConfig.fieldWidth / 2, 0, currentConfig.fieldHeight / 2);
        }
    }
}
