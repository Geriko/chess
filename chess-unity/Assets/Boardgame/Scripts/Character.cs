using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geriko.Boardgame
{
    public class Character : MonoBehaviour
    {
        internal int playerId;
        [SerializeField]
        private List<Action> actions;
        internal Vector2Int Coordinate { get { return currentFieldCoordinate; } }
        private Vector2Int currentFieldCoordinate;

        internal virtual List<Action> GetActions()
        {
            return actions;
        }

        public virtual void Init()
        {

        }

        internal void SetFieldCoordinate(Vector2Int fieldCoordinate)
        {
            currentFieldCoordinate = fieldCoordinate;
        }
    }
}
