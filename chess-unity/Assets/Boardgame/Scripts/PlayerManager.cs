using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geriko.Boardgame
{
    public class PlayerManager : MonoBehaviour
    {
        public GameObject PlayerPrefab;
        public List<Player> Players { get { return players; } }
        private List<Player> players;
        
        public virtual void CreatePlayer(PlayerConfig playerConfig)
        {
            if(players == null){
                players = new List<Player>();
            }

            GameObject playerGo = Instantiate(PlayerPrefab, this.transform);
            Player player = playerGo.GetComponent<Player>();
            player.InitPlayer(playerConfig);

            players.Add(player);
        }
    }
}
