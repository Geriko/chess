using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geriko.Boardgame
{
    [CreateAssetMenu(fileName = "Config", menuName = "Geriko/Boardgame/Config", order = 0)]
    public class Config : ScriptableObject
    {
        [Header("Map")]
        public GameObject fieldPrefab;
        public int boardWidth;
        public int boardHeight;
        public float fieldWidth;
        public float fieldHeight;
        public float boardPadding;

        [Header("Characters")]
        public List<GameObject> characters;
        public List<PrePositionedCharacter> prePositionedCharacters;

        [Header("Players")]
        public List<PlayerConfig> players;

    }

    [System.Serializable]
    public class PrePositionedCharacter
    {
        public GameObject prefab;
        public Vector2Int position;
        public int PlayerId;
    }

    [System.Serializable]
    public class PlayerConfig
    {
        public int id;
    }
}
