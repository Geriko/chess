using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geriko.Boardgame
{
    [RequireComponent(typeof(Camera))]
    public class PerspectiveGameCamera : GameCamera
    {
        private void Awake()
        {
            if (!Camera)
                Camera = GetComponent<Camera>();
        }

        public override void InitCamera(float boardWidth, float boardHeight, float fieldHeight, float fieldWidth, float padding)
        {
            if (!Camera)
                Camera = GetComponent<Camera>();
            return;
        }
    }
}
