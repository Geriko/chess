using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geriko.Boardgame
{
    [RequireComponent(typeof(Camera))]
    public class GameCamera : MonoBehaviour
    {
        public Camera Camera;

        private void Awake()
        {
            if(!Camera)
                Camera = GetComponent<Camera>();
        }

        public virtual void InitCamera(float boardWidth, float boardHeight, float fieldHeight,float fieldWidth, float padding)
        {
            if (!Camera)
                Camera = GetComponent<Camera>();

            transform.localPosition = new Vector3(boardWidth * fieldWidth / 2, transform.position.y, boardHeight * fieldHeight / 2);
            Camera.orthographicSize = boardWidth + padding;
        }
    }
}
