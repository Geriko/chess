using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geriko.Boardgame.Input
{
    public class InputManager : MonoBehaviour
    {
        BoardgameInput boardgameInput;
        GameController gameController;

        private void Awake()
        {
            boardgameInput = new BoardgameInput();
        }

        private void OnEnable()
        {
            boardgameInput.Enable();
        }

        private void OnDisable()
        {
            boardgameInput.Disable();
        }

        private void Start()
        {
            gameController = GameController.Instance;

            boardgameInput.Screen.ScreenInput.performed += _ => ScreenTouch();
        }

        private void ScreenTouch()
        {
            Vector2 inputPosition = boardgameInput.Screen.ScreenInputPosition.ReadValue<Vector2>();
            gameController.OnScreenTouch(inputPosition);
        }
    }
}
