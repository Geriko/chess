using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geriko.Boardgame.Chess
{
    public class King : ChessPiece
    {
        public override void Init()
        {
            base.Init();

            //Create movement demo
            List<Action> movements = GetActions();

            LinearChessMoveGroup movement = new LinearChessMoveGroup();
            movement.cost = 1;
            movement.minDistance = 1;
            movement.maxDistance = 1;
            movement.blockable = Blockable.Everything;
            movement.strikeEnemy = true;

            movement.directions = new List<Vector2Int>();
            movement.directions.Add(new Vector2Int(1, 0));
            movement.directions.Add(new Vector2Int(-1, 0));
            movement.directions.Add(new Vector2Int(0, 1));
            movement.directions.Add(new Vector2Int(0, -1));
            movement.directions.Add(new Vector2Int(1, 1));
            movement.directions.Add(new Vector2Int(-1, -1));
            movement.directions.Add(new Vector2Int(-1, 1));
            movement.directions.Add(new Vector2Int(1, -1));

            movements.Add(movement);
        }

        internal override List<Action> GetActions()
        {
            List<Action> actions = base.GetActions();
            if (actions != null && actions.Count > 0)
            {
                actions = actions.Copy();

                #region Castle
                /*
                if (firstStep)
                {
                    bool isWhite = IsWhite();
                    Vector2Int coordinate = isWhite ? new Vector2Int(Coordinate.x + 3, Coordinate.y) : new Vector2Int(Coordinate.x - 3, Coordinate.y);

                    Field possibleRookField = GameController.Instance.boardManager.GetCurrentFieldOf(coordinate);
                    if (possibleRookField)
                    {
                        Character rightChar = possibleRookField.GetCharacter();
                        if (rightChar != null && rightChar.GetType() == typeof(Rook))
                        {
                            Rook targetRook = (Rook)rightChar;

                            if (targetRook.firstStep)
                            {
                                Castle castle = new Castle(targetRook);
                                castle.cost = 1;
                                castle.minDistance = 3;
                                castle.maxDistance = 4;

                                castle.directions = new List<Vector2Int>();
                                castle.directions.Add(new Vector2Int(1, 0));
                                castle.directions.Add(new Vector2Int(-1, 0));

                                actions.Add(castle);
                            }
                        }
                    }
                }
                */
                #endregion
            }
            return actions;
        }
    }
}
