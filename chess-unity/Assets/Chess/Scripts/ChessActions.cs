using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geriko.Boardgame.Chess
{
    internal class Strike : LinearMovement
    {
        public override List<Field> PossibleFields(Character character, Field currentField, Field[,] fields, Config config)
        {
            List<Field> result = new List<Field>();
            for (int j = minDistance; j <= maxDistance; j++)
            {
                int x = currentField.Coordinate.x + (direction.x * j);
                int y = currentField.Coordinate.y + (direction.y * j);
                if (fields.IsExist(x, y))
                {
                    Character characterOnNextField = fields[x, y].GetCharacter();
                    if (characterOnNextField != null)
                    {
                        bool isEnemy = characterOnNextField.playerId != character.playerId;
                        switch (blockable)
                        {
                            case Blockable.None:
                                break;
                            case Blockable.Enemy:
                                if (isEnemy)
                                {
                                    return result;
                                }
                                break;
                            case Blockable.Friendly:
                                if (!isEnemy)
                                {
                                    return result;
                                }
                                break;
                            case Blockable.Everything:
                                return result;
                        }
                        if (isEnemy)
                            result.Add(fields[x, y]);
                    }
                }
            }
            return result;
        }

        internal override void Excecute(Character character, Field targetField)
        {
            ChessPiece targetChessPiece = (ChessPiece)targetField.GetCharacter();
            targetChessPiece.StrikeDown();
            GameController.Instance.boardManager.DeatachCharacter(targetField.GetCharacter());
            ((ChessPiece)character).SetIsFirstStep(false);
            base.Excecute(character, targetField);
        }
    }

    internal class PawnMove : LinearMovement
    {
        internal override void Excecute(Character character, Field targetField)
        {
            Field field = GameController.Instance.boardManager.GetCurrentFieldOf(character.Coordinate);
            bool enPassantable = Math.Abs(Field.Distance(field, targetField).y) == 2;
            base.Excecute(character, targetField);
            ((ChessPiece)character).SetIsFirstStep(false);
            if (enPassantable)
            {
                ((Pawn)character).MarkAsEnPassantable();
            }
        }
    }

    internal class EnPassant : LinearMovement
    {
        private Pawn targetPawn;

        public EnPassant(Pawn target)
        {
            targetPawn = target;
        }

        internal override void Excecute(Character character, Field targetField)
        {
            targetPawn.StrikeDown();
            GameController.Instance.boardManager.DeatachCharacter(targetField.GetCharacter());
            base.Excecute(character, targetField);
        }
    }

    internal class LinearChessMoveGroup : LinearMovementGroup
    {
        internal bool strikeEnemy;
        
        public override List<Field> PossibleFields(Character character, Field currentField, Field[,] fields, Config config)
        {
            List<Field> result = new List<Field>();
            foreach (Vector2Int direction in directions)
            {
                for (int j = minDistance; j <= maxDistance; j++)
                {
                    int x = currentField.Coordinate.x + (direction.x * j);
                    int y = currentField.Coordinate.y + (direction.y * j);
                    if (fields.IsExist(x, y))
                    {
                        Character characterOnNextField = fields[x, y].GetCharacter();
                        if (characterOnNextField != null)
                        {
                            bool isDone = false;
                            bool isEnemy = characterOnNextField.playerId != character.playerId;
                            switch (blockable)
                            {
                                case Blockable.None:
                                    break;
                                case Blockable.Enemy:
                                    if (isEnemy)
                                    {
                                        if (strikeEnemy)
                                        {
                                            result.Add(fields[x, y]);
                                        }
                                        isDone = true;
                                        break;
                                    }
                                    break;
                                case Blockable.Friendly:
                                    if (!isEnemy)
                                    {
                                        isDone = true;
                                        break;
                                    }
                                    break;
                                case Blockable.Everything:
                                    if (isEnemy)
                                    {
                                        if (strikeEnemy)
                                        {
                                            result.Add(fields[x, y]);
                                        }
                                        isDone = true;
                                        break;
                                    }
                                    isDone = true;
                                    break;
                            }
                            if (isDone)
                            {
                                break;
                            }
                        }
                        result.Add(fields[x, y]);
                    }
                }
            }
            return result;
        }

        internal override void Excecute(Character character, Field targetField)
        {
            ChessPiece targetChessPiece = (ChessPiece)targetField.GetCharacter();
            if (targetChessPiece)
            {
                targetChessPiece.StrikeDown();
                GameController.Instance.boardManager.DeatachCharacter(targetField.GetCharacter());
            }
            ((ChessPiece)character).SetIsFirstStep(false);
            base.Excecute(character, targetField);
        }
    }

    internal class ChessJump : JumpPositionGroup
    {
        internal override bool Jumpable(Field field, Character character)
        {
            Character fieldCharacter = field.GetCharacter();
            if(fieldCharacter != null)
            {
                bool isEnemy = fieldCharacter.playerId != character.playerId;
                if (!isEnemy)
                {
                    return false;
                }
            }
            return true;
        }
        
        internal override void Excecute(Character character, Field targetField)
        {
            ChessPiece targetChessPiece = (ChessPiece)targetField.GetCharacter();
            if (targetChessPiece)
            {
                targetChessPiece.StrikeDown();
                GameController.Instance.boardManager.DeatachCharacter(targetField.GetCharacter());
            }
            ((ChessPiece)character).SetIsFirstStep(false);
            base.Excecute(character, targetField);
        }
    }

    internal class Castle : LinearMovementGroup
    {
        private Rook targetRook;

        public Castle(Rook target)
        {
            targetRook = target;
        }

        internal override void Excecute(Character character, Field targetField)
        {
            Vector2Int king;
            Vector2Int rook;
            int rookDirection = character.Coordinate.x - targetRook.Coordinate.x;

            if (((ChessPiece)character).IsWhite())
            {
                if (rookDirection > 0)
                {
                    king = new Vector2Int(6, 0);
                    rook = new Vector2Int(5, 0);
                }
                else
                {
                    king = new Vector2Int(6, 0);
                    rook = new Vector2Int(5, 0);
                }
            }
            else
            {
                if (rookDirection > 0)
                {
                    king = new Vector2Int(1, 7);
                    rook = new Vector2Int(2, 7);
                }
                else
                {
                    king = new Vector2Int(1, 7);
                    rook = new Vector2Int(2, 7);
                }
            }
            BoardManager boardManager = GameController.Instance.boardManager;

            Field kingField = boardManager.GetCurrentFieldOf(king);
            boardManager.DeatachCharacter(character);
            character.transform.position = GameController.Instance.boardManager.CenterOfField(kingField);
            kingField.AddCharacter(character);


            Field rookField = boardManager.GetCurrentFieldOf(rook);
            boardManager.DeatachCharacter(targetRook);
            targetRook.transform.position = GameController.Instance.boardManager.CenterOfField(rookField);
            rookField.AddCharacter(character);
        }
    }
}
