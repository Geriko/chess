using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geriko.Boardgame.Chess
{
    public class Bishop : ChessPiece
    {
        public override void Init()
        {
            base.Init();

            //Create movement demo
            List<Action> movements = GetActions();

            LinearChessMoveGroup movement = new LinearChessMoveGroup();
            movement.cost = 1;
            movement.minDistance = 1;
            movement.maxDistance = 7;
            movement.blockable = Blockable.Everything;
            movement.strikeEnemy = true;

            movement.directions = new List<Vector2Int>();
            movement.directions.Add(new Vector2Int(1, 1));
            movement.directions.Add(new Vector2Int(-1, -1));
            movement.directions.Add(new Vector2Int(-1, 1));
            movement.directions.Add(new Vector2Int(1, -1));

            movements.Add(movement);
        }

        internal override List<Action> GetActions()
        {
            List<Action> actions = base.GetActions();
            return actions;
        }
    }
}