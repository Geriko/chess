using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Geriko.Boardgame.Chess
{
    public class ChessBoardManager : BoardManager
    {
        internal bool ValidateAction(Action action, Field targetField)
        {
            //Setting future character positions to test Chess
            Character ch = GameController.Instance.GetSelectedCharacter();
            Character targetFieldCh = targetField.GetCharacter();
            Field currentFied = Fields[ch.Coordinate.x, ch.Coordinate.y];

            currentFied.RemoveCharacter();
            targetField.AddCharacter(ch);

            if (targetFieldCh != null)
            {
                ((ChessPiece)targetFieldCh).IsDead = true;
            }

            bool result = !EnemyCanStrikeTheKing();

            //Setting back original character positions
            targetField.RemoveCharacter();
            currentFied.AddCharacter(ch);
            if(targetFieldCh != null)
            {
                targetField.AddCharacter(targetFieldCh);
                ((ChessPiece)targetFieldCh).IsDead = false;
            }

            return result;
        }

        private bool EnemyCanStrikeTheKing()
        {
            GameController gameController = GameController.Instance;
            Player currentPlayer = gameController.roundManager.CurrentPlayer;
            Player opponent = gameController.GetNextPlayer();

            King king = (King)currentPlayer.GetCharacters().First(x => x.GetType() == typeof(King));
            foreach (Character ch in opponent.GetCharacters())
            {
                if (!((ChessPiece)ch).IsDead)
                {
                    foreach (Movement action in ch.GetActions())
                    {
                        if (action.PossibleFields(ch, Fields[ch.Coordinate.x, ch.Coordinate.y], Fields, GameController.Instance.GetConfig()).Contains(Fields[king.Coordinate.x, king.Coordinate.y]))
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
    }
}
