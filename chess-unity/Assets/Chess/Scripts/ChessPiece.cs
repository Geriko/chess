using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace Geriko.Boardgame.Chess
{
    public class ChessPiece : Character
    {
        public static Material White, Black;
        private static bool HaveMaterials = false;

        public Renderer gfxRenderer;
        internal bool firstStep = true;

        internal bool IsDead { get; set; }

        public override void Init()
        {
            base.Init();
            CreateChessCharacterMaterials();
            InitMaterial();
        }

        private void InitMaterial()
        {
            if (IsWhite())
            {
                gfxRenderer.material = White;
            }
            else
            {
                gfxRenderer.material = Black;
            }
        }

        private void CreateChessCharacterMaterials()
        {
            if(GameController.Instance.GetConfig().GetType() != typeof(ChessConfig))
            {
                throw new InvalidOperationException("The config must be ChessConfig");
            }
            ChessConfig config = (ChessConfig)GameController.Instance.GetConfig();
            if (!HaveMaterials)
            {
                White = new Material(config.characterShader);
                White.SetColor("_MainColor", config.characterWhite);
                White.SetColor("_OutlineColor", config.characterBlack);
                White.SetFloat("_Outline", config.characterOutlineWidth);
                Black = new Material(config.characterShader);
                Black.SetColor("_MainColor", config.characterBlack);
                Black.SetColor("_OutlineColor", config.characterWhite);
                Black.SetFloat("_Outline", config.characterOutlineWidth);
                HaveMaterials = true;
            }
        }
        

        internal void SetIsFirstStep(bool v)
        {
            firstStep = v;
        }

        internal void StrikeDown()
        {
            Destroy(this.gameObject);
            IsDead = true;
        }

        internal bool IsWhite()
        {
            return playerId % 2 == 0;
        }
    }
}