using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geriko.Boardgame.Chess
{
    public class ChessField : Field
    {
        public static Material White, Black;
        private static bool HaveMaterials = false;
        public Renderer fieldRenderer;

        public override void InitField(int x, int y)
        {
            base.InitField(x, y);

            if (!HaveMaterials)
            {
                CreateChessBoardMaterials();
            }

            if (y % 2 == 0)
                fieldRenderer.material = (x % 2 == 0) ? Black : White;
            else
                fieldRenderer.material = (x % 2 == 0) ? White : Black;
        }

        private void CreateChessBoardMaterials()
        {
            if (GameController.Instance.GetConfig().GetType() != typeof(ChessConfig))
            {
                throw new InvalidOperationException("The config must be ChessConfig");
            }
            ChessConfig config = (ChessConfig)GameController.Instance.GetConfig();

            White = new Material(config.fieldShader);
            White.color = config.fieldWhite;
            Black = new Material(config.fieldShader);
            Black.color = config.fieldBlack;
            HaveMaterials = true;
        }

        internal override void Highlight(Action action)
        {
            if (((ChessBoardManager)GameController.Instance.boardManager).ValidateAction(action, this))
            {
                base.Highlight(action);
            }
        }
    }
}
