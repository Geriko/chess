using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geriko.Boardgame.Chess
{
    public class Pawn : ChessPiece
    {
        public bool enPassantable = false;
        private int turnsSinceEnPassantable = 0;

        public override void Init()
        {
            base.Init();
            
            //Create movement demo
            List<Action> movements = GetActions();

            PawnMove movement = new PawnMove();
            movement.cost = 1;
            movement.minDistance = 1;
            movement.maxDistance = 1;
            movement.blockable = Blockable.Everything;

            Strike strikeLeft = new Strike();
            strikeLeft.cost = 1;
            strikeLeft.minDistance = 1;
            strikeLeft.maxDistance = 1;
            strikeLeft.blockable = Blockable.Friendly;

            Strike strikeRight = new Strike();
            strikeRight.cost = 1;
            strikeRight.minDistance = 1;
            strikeRight.maxDistance = 1;
            strikeRight.blockable = Blockable.Friendly;

            if (IsWhite())
            {
                movement.direction = new Vector2Int(0, 1);
                strikeLeft.direction = new Vector2Int(-1, 1);
                strikeRight.direction = new Vector2Int(1, 1);
            }
            else
            {
                movement.direction = new Vector2Int(0, -1);
                strikeLeft.direction = new Vector2Int(-1, -1);
                strikeRight.direction = new Vector2Int(1, -1);
            }

            movements.Add(movement);
            movements.Add(strikeLeft);
            movements.Add(strikeRight);
        }

        internal override List<Action> GetActions()
        {
            List<Action> actions = base.GetActions();
            if (actions.Count > 0)
            {
                actions = actions.Copy();
                if (firstStep)
                {
                    //move distance increased to 2 on first step
                    ((LinearMovement)actions[0]).maxDistance = 2;
                }
                else
                {
                    ((LinearMovement)actions[0]).maxDistance = 1;

                    #region En Passant
                    Vector2Int right = new Vector2Int(Coordinate.x + 1, Coordinate.y);
                    Vector2Int left = new Vector2Int(Coordinate.x - 1, Coordinate.y);

                    Field rightField = GameController.Instance.boardManager.GetCurrentFieldOf(right);
                    Field leftField = GameController.Instance.boardManager.GetCurrentFieldOf(left);
                    if (rightField)
                    {
                        Character rightChar = rightField.GetCharacter();
                        if (rightChar != null && rightChar.GetType() == typeof(Pawn))
                        {
                            Pawn targetPawn = (Pawn)rightChar;
                            if (targetPawn.enPassantable)
                            {
                                EnPassant enPassant = new EnPassant(targetPawn);
                                enPassant.cost = 1;
                                enPassant.minDistance = 1;
                                enPassant.maxDistance = 1;

                                if (IsWhite())
                                {
                                    enPassant.direction = new Vector2Int(1, 1);
                                }
                                else
                                {
                                    enPassant.direction = new Vector2Int(1, -1);
                                }

                                actions.Add(enPassant);
                            }
                        }
                    }
                    if (leftField)
                    {
                        Character leftChar = leftField.GetCharacter();
                        if (leftChar != null && leftChar.GetType() == typeof(Pawn))
                        {
                            Pawn targetPawn = (Pawn)leftChar;
                            if (targetPawn.enPassantable)
                            {
                                EnPassant enPassant = new EnPassant(targetPawn);
                                enPassant.cost = 1;
                                enPassant.minDistance = 1;
                                enPassant.maxDistance = 1;

                                if (IsWhite())
                                {
                                    enPassant.direction = new Vector2Int(-1, 1);
                                }
                                else
                                {
                                    enPassant.direction = new Vector2Int(-1, -1);
                                }

                                actions.Add(enPassant);
                            }
                        }
                    }
                    #endregion
                }
            }
            return actions;
        }

        internal void MarkAsEnPassantable()
        {
            enPassantable = true;
            turnsSinceEnPassantable = 0;
            GameController.Instance.roundManager.OnEndOfRound += RemoveEnpassantMark;
        }

        private void RemoveEnpassantMark()
        {
            turnsSinceEnPassantable++;
            if(turnsSinceEnPassantable > 1)
            {
                enPassantable = false;
                GameController.Instance.roundManager.OnEndOfRound -= MarkAsEnPassantable;
            }
        }
    }
}