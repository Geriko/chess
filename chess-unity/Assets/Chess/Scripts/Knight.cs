using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geriko.Boardgame.Chess
{
    public class Knight : ChessPiece
    {
        public override void Init()
        {
            base.Init();

            //Create movement demo
            List<Action> actions = GetActions();

            ChessJump movement = new ChessJump();
            movement.cost = 1;

            movement.jumpPositions = new List<Vector2Int>();

            movement.jumpPositions.Add(new Vector2Int(2, 1));
            movement.jumpPositions.Add(new Vector2Int(2, -1));
            movement.jumpPositions.Add(new Vector2Int(-2, -1));
            movement.jumpPositions.Add(new Vector2Int(-2, 1));
            
            movement.jumpPositions.Add(new Vector2Int(1, 2));
            movement.jumpPositions.Add(new Vector2Int(1, -2));
            movement.jumpPositions.Add(new Vector2Int(-1, -2));
            movement.jumpPositions.Add(new Vector2Int(-1, 2));

            actions.Add(movement);
        }

        internal override List<Action> GetActions()
        {
            List<Action> actions = base.GetActions();
            return actions;
        }
    }
}
