using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Geriko.Boardgame.Chess
{
    [CreateAssetMenu(fileName = "Config", menuName = "Geriko/Boardgame/ChessConfig", order = 1)]
    public class ChessConfig : Config
    {
        public Shader characterShader;
        public Color characterWhite;
        public Color characterBlack;
        public float characterOutlineWidth = 0.002f;

        public Shader fieldShader;
        public Color fieldWhite;
        public Color fieldBlack;
    }
}